package com.mindvalley.androidtest;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.mindvalley.androidtest.model.Product;
import com.mindvalley.androidtest.util.AppController;
import com.mindvalley.androidtest.util.ParseJson;

import org.json.JSONArray;

import java.util.ArrayList;

public class MainActivity extends ActivityBase
{
    private  Toolbar toolbar;

    private RecyclerView rvListData;

    private ArrayList<Product> alProduct;

    private SwipeRefreshLayout swipeRefreshLayout;

    private AdapterProductList adpProdList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initData();

        initUi();

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);

                loadData();
            }
        });


    }

    protected void initData()
    {
        super.initData();

        alProduct = new ArrayList<>();
    }

    private void initUi()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        rvListData = (RecyclerView) findViewById(R.id.rvListData);

        rvListData.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        adpProdList = new AdapterProductList();

        rvListData.setAdapter(adpProdList);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                loadData();
            }
        });

        findViewById(R.id.fabAdd).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });
    }

    public class AdapterProductList extends RecyclerView.Adapter<AdapterProductList.MyViewHolder>
    {
        public class MyViewHolder extends RecyclerView.ViewHolder
        {
            private LinearLayout llMain;

            private ImageView ivProductImage;

            private RoundedImageView ivProfile;

            private TextView tvUserName;

            private TextView tvLikeCount;

            private LinearLayout llUserDeatil;

            //private TextView tvComment;

            //private TextView tvName;

            public MyViewHolder(View view)
            {
                super(view);

                llMain = (LinearLayout) view.findViewById(R.id.llMain);

                llUserDeatil = (LinearLayout) view.findViewById(R.id.llUserDeatil);

                tvUserName = (TextView) view.findViewById(R.id.tvUserName);

                tvLikeCount = (TextView) view.findViewById(R.id.tvLikeCount);

                //tvName = (TextView) view.findViewById(R.id.tvName);

                ivProfile = (RoundedImageView) view.findViewById(R.id.ivProfile);

                ivProductImage = (ImageView) view.findViewById(R.id.ivProductImage);
            }
        }

        public AdapterProductList()
        {
            //this.moviesList = moviesList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_product, parent, false);

            //itemView.setOnClickListener(mOnClickListener);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position)
        {
            holder.ivProductImage.setBackgroundColor(Color.parseColor(alProduct.get(position).getColor()));

            ImageLoader imageLoader = AppController.getInstance().getImageLoader();

            imageLoader.get(alProduct.get(position).getUser().getImageMedium(), ImageLoader.getImageListener(
                    holder.ivProfile, R.drawable.icon_no_image_placeholder, R.drawable.icon_no_image_placeholder));

            // If you are using normal ImageView
            if(AppConstants.DEBUG) Log.v(AppConstants.DEBUG_TAG, "IMAGE URL : "+position + "::" + alProduct.get(position).getThumb());
            imageLoader.get(alProduct.get(position).getRegular(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (AppConstants.DEBUG)
                        Log.v(AppConstants.DEBUG_TAG, "Image Load Error: " + error.getMessage());
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivProductImage.setImageBitmap(response.getBitmap());
                    }
                }
            });

            holder.tvUserName.setText(alProduct.get(position).getUser().getName());

            holder.tvLikeCount.setText(String.valueOf(alProduct.get(position).getLikes()));

            holder.llMain.setOnClickListener(mOnClickListener);

            holder.llUserDeatil.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    goToUserProfileScreen(alProduct.get(position).getUser());
                }
            });

            holder.ivProductImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToProductDetailScreen(alProduct.get(position));
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return alProduct.size();
        }
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            int itemPosition = rvListData.getChildLayoutPosition(view);
        }
    };

    private void loadData()
    {
        String tag_json_obj = "json_obj_req";

        JsonArrayRequest req = new JsonArrayRequest(AppConstants.URL_GET_DATA, new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response)
            {
                if(AppConstants.DEBUG) Log.v(AppConstants.DEBUG_TAG, "RESPONSE : "+response.toString());

                swipeRefreshLayout.setRefreshing(false);

                try
                {
                    alProduct.clear();

                    alProduct.addAll(ParseJson.parseProductList(response.toString()));

                    if(AppConstants.DEBUG) Log.v(AppConstants.DEBUG_TAG, "PRODUCT DATA SIZE : "+alProduct.size());

                    adpProdList.notifyDataSetChanged();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },
        new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if(AppConstants.DEBUG) Log.v(AppConstants.DEBUG_TAG, "Error: " + error.getMessage());

                swipeRefreshLayout.setRefreshing(false);
            }
        });

        AppController.getInstance().addToRequestQueue(req, tag_json_obj);
    }

}
