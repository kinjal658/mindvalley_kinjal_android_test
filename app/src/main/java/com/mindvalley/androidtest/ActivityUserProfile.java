package com.mindvalley.androidtest;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.mindvalley.androidtest.model.UserData;
import com.mindvalley.androidtest.util.AppController;

public class ActivityUserProfile extends ActivityBase
{
    private UserData user;

    private Toolbar toolbar;

    private ImageView ivProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_profile);

        initData();

        initUi();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_from_left_to_right, R.anim.slide_out_from_left_to_right);

        finish();
    }

    protected void initData()
    {
        super.initData();

        user = new UserData();

        user = (UserData) getIntent().getSerializableExtra("SELECTED_USER_DATA");

        if(AppConstants.DEBUG) Log.v(AppConstants.DEBUG_TAG, "USER DETAILS : " + user.getName());
    }

    private void initUi()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapsingToolbar.setExpandedTitleColor(Color.BLACK);
        collapsingToolbar.setTitle(user.getName());

        ivProfile = (ImageView) findViewById(R.id.ivProfile);

        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        imageLoader.get(user.getImageLarge(), ImageLoader.getImageListener(
                ivProfile, R.drawable.icon_profile_default, R.drawable.icon_profile_default));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            onBackPressed();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
