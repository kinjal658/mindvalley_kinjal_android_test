package com.mindvalley.androidtest;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.mindvalley.androidtest.model.Product;
import com.mindvalley.androidtest.util.AppController;

import java.text.SimpleDateFormat;

public class ActivityProductDetail extends ActivityBase
{
    private Toolbar toolbar;

    private Product product;

    private RoundedImageView ivProductImg, ivProfile;

    private TextView tvLikeCount, tvUserName, tvCategories;

    private ImageView ivLike, ivShare;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product_detail);

        initData();

        initUi();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_from_left_to_right, R.anim.slide_out_from_left_to_right);

        finish();
    }

    protected void initData()
    {
        super.initData();

        product = new Product();

        product = (Product) getIntent().getSerializableExtra("SELECTED_PRODUCT");
    }

    private void initUi()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);

        ivProductImg = (RoundedImageView) findViewById(R.id.ivProductImg);

        ivProfile = (RoundedImageView) findViewById(R.id.ivProfile);

        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        imageLoader.get(product.getUser().getImageMedium(), ImageLoader.getImageListener(
                ivProfile, R.drawable.icon_no_image_placeholder, R.drawable.icon_no_image_placeholder));

        imageLoader.get(product.getRegular(), ImageLoader.getImageListener(
                ivProductImg, R.drawable.icon_no_image_placeholder, R.drawable.icon_no_image_placeholder));

        tvLikeCount = (TextView) findViewById(R.id.tvLikeCount);
        tvLikeCount.setText(String.valueOf(product.getLikes()));

        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvUserName.setText(product.getUser().getName());

        tvCategories = (TextView) findViewById(R.id.tvCategories);

        if(product.getAlCategory().size() > 0)
        {
            String categories = "";

            for(int i = 0; i < product.getAlCategory().size(); i++)
            {
                categories = categories.concat(", ").concat(product.getAlCategory().get(i).getTitle());
            }

            String categoryValue = "<b>Categories:</b>".concat(" ").concat(categories.substring(1));

            tvCategories.setText(Html.fromHtml(categoryValue));
        }

        try
        {
            //2016-05-29T15:42:02-04:00
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sszzz");
            //String date = dateFormat.format(product.getCreatedAt());

            if(AppConstants.DEBUG) Log.v(AppConstants.DEBUG_TAG, "DATE FORMAT : "+sdf.format(product.getCreatedAt()));
            //String date = sdf.format()
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        findViewById(R.id.llUserDeatil).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                goToUserProfileScreen(product.getUser());
            }
        });

        ivLike = (ImageView) findViewById(R.id.ivLike);

        ivShare = (ImageView) findViewById(R.id.ivShare);

        ivLike.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Animation animationScaleUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pop_in);
                Animation animationScaleDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pop_out);

                AnimationSet growShrink = new AnimationSet(true);
                growShrink.addAnimation(animationScaleUp);
                growShrink.addAnimation(animationScaleDown);
                ivLike.startAnimation(growShrink);
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animationScaleUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pop_in);
                Animation animationScaleDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pop_out);

                AnimationSet growShrink = new AnimationSet(true);
                growShrink.addAnimation(animationScaleUp);
                growShrink.addAnimation(animationScaleDown);
                ivShare.startAnimation(growShrink);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            onBackPressed();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
