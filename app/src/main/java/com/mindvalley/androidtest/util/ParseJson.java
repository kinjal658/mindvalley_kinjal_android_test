package com.mindvalley.androidtest.util;

import com.mindvalley.androidtest.model.Category;
import com.mindvalley.androidtest.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParseJson
{
    public static ArrayList<Product> parseProductList(String response) throws JSONException
    {
        ArrayList<Product> alProduct = new ArrayList<>();

        JSONArray jsnArr = new JSONArray(response);

        Product product;

        Category category;

        for(int i = 0; i < jsnArr.length(); i++)
        {
            product = new Product();

            JSONObject jsonProduct = jsnArr.getJSONObject(i);

            if(jsonProduct.has("id"))
                product.setId(jsonProduct.getString("id"));

            if(jsonProduct.has("created_at"))
                product.setCreatedAt(jsonProduct.getString("created_at"));

            if(jsonProduct.has("width"))
                product.setWidth(jsonProduct.getInt("width"));

            if(jsonProduct.has("height"))
                product.setHeight(jsonProduct.getInt("height"));

            if(jsonProduct.has("color"))
                product.setColor(jsonProduct.getString("color"));

            if(jsonProduct.has("likes"))
                product.setLikes(jsonProduct.getInt("likes"));

            if(jsonProduct.has("user"))
            {
               JSONObject jsonUser = jsonProduct.getJSONObject("user");

                if(jsonUser.has("id"))
                    product.getUser().setId(jsonUser.getString("id"));

                if(jsonUser.has("username"))
                    product.getUser().setUserName(jsonUser.getString("username"));

                if(jsonUser.has("name"))
                    product.getUser().setName(jsonUser.getString("name"));

                if(jsonUser.has("profile_image"))
                {
                    JSONObject jsonImage = jsonUser.getJSONObject("profile_image");

                    if(jsonImage.has("small"))
                        product.getUser().setImageSmall(jsonImage.getString("small"));

                    if(jsonImage.has("medium"))
                        product.getUser().setImageMedium(jsonImage.getString("medium"));

                    if(jsonImage.has("large"))
                        product.getUser().setImageLarge(jsonImage.getString("large"));
                }

                if(jsonUser.has("links"))
                {
                    JSONObject jsonLinks = jsonUser.getJSONObject("links");

                    if(jsonLinks.has("self"))
                        product.getUser().setLinkSelf(jsonLinks.getString("self"));

                    if(jsonLinks.has("html"))
                        product.getUser().setLinkHtml(jsonLinks.getString("html"));

                    if(jsonLinks.has("photos"))
                        product.getUser().setLinkPhotos(jsonLinks.getString("photos"));

                    if(jsonLinks.has("likes"))
                        product.getUser().setLinkLikes(jsonLinks.getString("likes"));
                }
            }

            if(jsonProduct.has("urls"))
            {
                JSONObject jsonUrls = jsonProduct.getJSONObject("urls");

                if(jsonUrls.has("raw"))
                    product.setRaw(jsonUrls.getString("raw"));

                if(jsonUrls.has("full"))
                    product.setFull(jsonUrls.getString("full"));

                if(jsonUrls.has("regular"))
                    product.setRegular(jsonUrls.getString("regular"));

                if(jsonUrls.has("small"))
                    product.setSmall(jsonUrls.getString("small"));

                if(jsonUrls.has("thumb"))
                    product.setThumb(jsonUrls.getString("thumb"));
            }

            if(jsonProduct.has("categories"))
            {
                JSONArray jsnArrCat = jsonProduct.getJSONArray("categories");

                for(int j = 0; j < jsnArrCat.length(); j++)
                {
                    JSONObject jsonCategory = jsnArrCat.getJSONObject(j);

                    category = new Category();

                    if(jsonCategory.has("id"))
                        category.setId(jsonCategory.getString("id"));

                    if(jsonCategory.has("title"))
                        category.setTitle(jsonCategory.getString("title"));

                    if(jsonCategory.has("photo_count"))
                        category.setPhotoCount(jsonCategory.getInt("photo_count"));

                    if(jsonCategory.has("self"))
                        category.setLinkSelf(jsonCategory.getString("self"));

                    if(jsonCategory.has("photos"))
                        category.setLinkPhotos(jsonCategory.getString("photos"));

                    product.getAlCategory().add(category);
                }
            }

            alProduct.add(product);
        }

        return alProduct;
    }
}
