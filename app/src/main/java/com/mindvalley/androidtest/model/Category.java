package com.mindvalley.androidtest.model;

import java.io.Serializable;

public class Category implements Serializable
{
    private String id = "";
    private String title = "";
    private int photoCount;
    private String linkSelf = "";
    private String linkPhotos = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    public String getLinkSelf() {
        return linkSelf;
    }

    public void setLinkSelf(String linkSelf) {
        this.linkSelf = linkSelf;
    }

    public String getLinkPhotos() {
        return linkPhotos;
    }

    public void setLinkPhotos(String linkPhotos) {
        this.linkPhotos = linkPhotos;
    }
}
