package com.mindvalley.androidtest.model;

import java.io.Serializable;

public class UserData implements Serializable
{
    private String id = "";
    private String name = "";
    private String userName = "";

    private String imageSmall = "";
    private String imageMedium = "";
    private String imageLarge = "";

    private String linkSelf = "";
    private String linkHtml = "";
    private String linkPhotos = "";
    private String linkLikes = "";

    private String urlRaw = "";
    private String urlFull = "";
    private String urlRegular = "";
    private String urlSmall = "";
    private String urlThumb = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageSmall() {
        return imageSmall;
    }

    public void setImageSmall(String imageSmall) {
        this.imageSmall = imageSmall;
    }

    public String getImageMedium() {
        return imageMedium;
    }

    public void setImageMedium(String imageMedium) {
        this.imageMedium = imageMedium;
    }

    public String getImageLarge() {
        return imageLarge;
    }

    public void setImageLarge(String imageLarge) {
        this.imageLarge = imageLarge;
    }

    public String getLinkSelf() {
        return linkSelf;
    }

    public void setLinkSelf(String linkSelf) {
        this.linkSelf = linkSelf;
    }

    public String getLinkHtml() {
        return linkHtml;
    }

    public void setLinkHtml(String linkHtml) {
        this.linkHtml = linkHtml;
    }

    public String getLinkPhotos() {
        return linkPhotos;
    }

    public void setLinkPhotos(String linkPhotos) {
        this.linkPhotos = linkPhotos;
    }

    public String getLinkLikes() {
        return linkLikes;
    }

    public void setLinkLikes(String linkLikes) {
        this.linkLikes = linkLikes;
    }

    public String getUrlRaw() {
        return urlRaw;
    }

    public void setUrlRaw(String urlRaw) {
        this.urlRaw = urlRaw;
    }

    public String getUrlFull() {
        return urlFull;
    }

    public void setUrlFull(String urlFull) {
        this.urlFull = urlFull;
    }

    public String getUrlRegular() {
        return urlRegular;
    }

    public void setUrlRegular(String urlRegular) {
        this.urlRegular = urlRegular;
    }

    public String getUrlSmall() {
        return urlSmall;
    }

    public void setUrlSmall(String urlSmall) {
        this.urlSmall = urlSmall;
    }

    public String getUrlThumb() {
        return urlThumb;
    }

    public void setUrlThumb(String urlThumb) {
        this.urlThumb = urlThumb;
    }
}
