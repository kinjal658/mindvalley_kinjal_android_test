package com.mindvalley.androidtest.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable
{
    private String id = "";
    private String createdAt = "";
    private int width;
    private int height;
    private String color = "";
    private int likes;

    private UserData user;
    private ArrayList<Category> alCategory;

    private String raw = "";
    private String full = "";
    private String regular = "";
    private String small = "";
    private String thumb = "";

    public Product()
    {
        user = new UserData();

        alCategory = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }

    public ArrayList<Category> getAlCategory() {
        return alCategory;
    }

    public void setAlCategory(ArrayList<Category> alCategory) {
        this.alCategory = alCategory;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
