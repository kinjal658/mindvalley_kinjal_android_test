package com.mindvalley.androidtest;

public class AppConstants
{
    public static final boolean DEBUG = true;

    public static final String DEBUG_TAG = "MINDVALLEY_APP";

    public static final String URL_GET_DATA = "http://pastebin.com/raw/wgkJgazE";
}
