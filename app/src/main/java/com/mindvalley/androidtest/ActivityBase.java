package com.mindvalley.androidtest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.mindvalley.androidtest.model.Product;
import com.mindvalley.androidtest.model.UserData;
import com.mindvalley.androidtest.util.AppController;

import java.text.SimpleDateFormat;

public class ActivityBase extends AppCompatActivity
{
    protected ProgressDialog pDialog;

    protected AppController appController;

    protected SimpleDateFormat dateFormat;

    protected void initData()
    {
        appController = AppController.getInstance();

        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    protected void goToUserProfileScreen(UserData userData)
    {
        Intent intent = new Intent(getApplicationContext(), ActivityUserProfile.class);

        intent.putExtra("SELECTED_USER_DATA", userData);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);

        startActivity(intent);
    }

    protected void goToProductDetailScreen(Product product)
    {
        Intent intent = new Intent(getApplicationContext(), ActivityProductDetail.class);

        intent.putExtra("SELECTED_PRODUCT", product);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);

        startActivity(intent);
    }

    protected void showProgressDialog()
    {
        if(pDialog != null && pDialog.isShowing())
            pDialog.dismiss();

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.txt_loading));
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
        pDialog.show();
    }

    protected void cancelProgressDialog()
    {
        if (pDialog != null)
        {
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
}
